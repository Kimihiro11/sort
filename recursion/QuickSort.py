__author__ = 'Thorne'

#快速排序
#基于二叉树
#无序的数组
def quicksort(arr):
    #基线条件
    if len(arr) < 2:
        return arr
    else:
        #递归条件
        pivot = arr[0]
        smaller = [ i for i in arr[1:] if i <= pivot]
        # smaller =[]
        # for i in arr[1:]:
        #     if i <= pivot:
        #         print(i)
        #         smaller.append(i)
        #print(smaller)
        bigger = [i for i in arr[1:] if i > pivot]
        #print(bigger)
        return quicksort(smaller) + [pivot] + quicksort(bigger)
print(quicksort([5,3,65,35,2,1,58]))