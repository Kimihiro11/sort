__author__ = 'Thorne'

#递归算法
#注意函数对内存的占用，
#调用栈可能很长，这将占用大量内存


def countdown(i):
    print(i)
    if i !=0:
     countdown(i-1)
    return 'finish'
print(countdown(5))

#D&C 基线和递归条件
def sum (arr):
    if arr == []:
        return 'finish'
    else:
        return arr[0] + sum(arr[1:])

def count(arr):
    if arr == []:
        return '0元素'
    else:
        return 1 + count(arr[1:])

def getMax(arr):
    if len(arr) == 2:
        return arr[0] if arr[0] > arr[1] else arr[1]
    else:
        arrMax = max(arr[1:])
        return arr[0] if arr[0] > arrMax else arrMax

arr=[1,2,3,4,5,6,8,7]
print(getMax(arr))