__author__ = 'Thorne'

#二分查找
#log2N 则x叫做以a为底N的对数,记做x=log(a)(N）
#算法运行时间不是以秒为单位
#算法运行时间是从器增速的角度度量的
#算法的运行时间用大O表示法表示
def binary_search(list ,item):
    low = 0
    high = len(list)-1

    while low <= high:
        mid = int((low + high)/2)
        guess = list[mid]
        if guess == item:
            return mid
        if guess > item:
            high = mid - 1
        else:
            low = mid + 1
    return  None

list = [1, 2, 3, 4, 5]
print(binary_search(list, 3))