__author__ = 'Thorne'

#广度优先算法。
#使用队列
#广度优先找出最短路径
#因为要按照加入顺序搜索列表，所以搜索列表一定是队列FIFO


graph = {}
graph['me']  = ['tom', 'poi', 'swt']
graph['tom'] = ['ssw', 'rng', 'skt']
graph['poi'] = ['edg']
graph['swt'] = ['lgd', 'rw']
graph['ssw'] = []
graph['rng'] = ['ppd']
graph['skt'] = []
graph['edg'] = []
graph['lgd'] = []
graph['rw'] = []
graph['ppd'] = []
#print(graph)
from collections import deque

def search_bfs(name):
    search_queue = deque()
    search_queue += graph[name]
    searched = []
    while search_queue:
        person = search_queue.popleft()
        if person not in searched:
            if target(person):
                print('it\'s him'+ ' '+person)
                return True
            else:
                search_queue += graph[person]
                searched.append(person)
    return False

def target(name):
    if 'd' in name:
        return True

search_bfs('me')
