__author__ = 'Thorne'

#选择排序

def findSmallest(arr):
    smallest = arr[0]
    smallest_index = 0
    for i in range(1, len(arr)):
        if arr[i] < smallest:
            smallest = arr[i]
            smallest_index = i
    return smallest_index

def selectionSort(arr):
    newArr = []
    for i in range(len(arr)):
        smallest = findSmallest(arr)

        newArr.append(arr.pop(smallest))

    return newArr
arr = [5,6,2,1,9,3,7,4]

print(selectionSort(arr))
